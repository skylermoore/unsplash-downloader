extern crate reqwest;

use std::fs::File;
use std::borrow::Cow;

pub enum Time {
    Daily,
    Weekly
}

pub enum Source {
    Search,
    Collection
}

pub struct Downloader<'a> {
    resolution: Option<&'a str>,
    output: Option<&'a str>,
    time: Option<Time>,
    source: Source,
}

impl<'a> Downloader<'a> {
    pub fn new() -> Downloader<'a> {
        Downloader{
            resolution: None,
            output: None,
            time: None,
            source: Source::Search
        }
    }

    pub fn set_resolution(&mut self, resolution: Option<&'a str>) {
        self.resolution = resolution;
    }

    pub fn set_output(&mut self, output: Option<&'a str>) {
        self.output = output;
    }

    pub fn set_time(&mut self, time: Option<Time>) {
        self.time = time;
    }

    pub fn set_source(&mut self, source: Source) {
        self.source = source;
    }

    //TODO: Check if Cow is the correct way to handle this
    fn process_output(filename: &'a str, output: Option<&'a str>) -> Cow<'a, str> {
        let out = match output {
            Some(value) => value,
            None => "."
        };

        if out.contains(".jpg") {
            return Cow::Borrowed(out);
        } else if out.ends_with("/") {
            return Cow::Owned([out, filename, ".jpg"].concat());
        } else {
            return Cow::Owned([out, "/", filename, ".jpg"].concat());
        }
    }

    //TODO: Look into a better/more formal way of string concatination
    pub fn download(&self, term: &str) {
        let resolution = match self.resolution {
            Some(res) => ["/", res].concat(),
            _ => "".to_owned()
        };

        let time = match self.time {
            Some(Time::Daily) => "/daily",
            Some(Time::Weekly) => "/weekly",
            None => ""
        };

        let url = match self.source  {
            Source::Collection => ["https://source.unsplash.com/collection/", term, time, resolution.as_str()].concat(),
            Source::Search => ["https://source.unsplash.com/featured", resolution.as_str(), time , "/?", term].concat()
        };

        let mut response = reqwest::blocking::get(url.as_str()).unwrap();
        let id = &response.url().path()[1..];

        //TODO: If the picture exists, attempt to download a new one
        //TODO: Check is Cow is the correct way to handle this
        let filename = Downloader::process_output(id, self.output);
        let path = filename.as_ref();
        let mut file = File::create(&path).unwrap(); //TODO: Handle Unwrap

        std::io::copy(&mut response, &mut file).unwrap(); //TODO: Handle Unwrap
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn process_output_raw() {
        let filename = "image";
        let output: Option<&str> = Some("output/");
        let full_name = Downloader::process_output(filename, output);
        assert_eq!("output/image.jpg", full_name);
    }

    #[test]
    fn process_output_filetype() {
        let filename = "image";
        let output: Option<&str> = Some("output/target.jpg");
        let full_name = Downloader::process_output(filename, output);
        assert_eq!("output/target.jpg", full_name);
    }
}
