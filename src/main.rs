extern crate clap;

mod downloader;

use clap::{App, Arg};
use self::downloader::{Downloader, Time, Source};

fn main() {
    let matches = App::new("Unsplash Downloader")
        .version("0.0.1")
        .author("Skyler Moore")
        .about("Unsplash Image Downloader")
        .arg(Arg::with_name("search") //Search
            .help("Search term")
            .long("search")
            .short("s")
            .takes_value(true)
            .value_name("TERM")
            .required_unless_one(&["collection"]))
        .arg(Arg::with_name("collection") //Collection
            .help("Collection Name")
            .long("collection")
            .short("c")
            .takes_value(true)
            .value_name("COLLECTION")
            .required_unless_one(&["search"]))
        .arg(Arg::with_name("resolution") //Resolution
            .help("Resolution to request ex: 1920x1080")
            .long("resolution")
            .short("r")
            .takes_value(true)
            .value_name("RESOLUTION"))
        .arg(Arg::with_name("output") //Output
            .help("Output directory")
            .long("output")
            .short("o")
            .takes_value(true)
            .value_name("OUTPUT"))
        .arg(Arg::with_name("time") //Time
            .help("Daily/Weekly option")
            .long("time")
            .short("t")
            .takes_value(true)
            .value_name("TIME")
            .possible_values(&["daily", "weekly"]))
        .get_matches();

    let mut downloader = Downloader::new();

    downloader.set_resolution(matches.value_of("resolution"));
    downloader.set_output(matches.value_of("output"));

    if let Some(time) = matches.value_of("time") {
        match time {
            "daily" => downloader.set_time(Some(Time::Daily)),
            "weekly" => downloader.set_time(Some(Time::Weekly)),
            _ => downloader.set_time(None)
        }
    }

    if let Some(term) = matches.value_of("search") {
        downloader.set_source(Source::Search);
        downloader.download(term)
        
    } else if let Some(collection) = matches.value_of("collection") {
        downloader.set_source(Source::Collection);
        downloader.download(collection);
    }
}
