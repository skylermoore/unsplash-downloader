# Read Me

CLI tool to download pictures from Unsplash.com

## Project Goals

The goal of this project is to allow an easy way to obtain pictures from Unsplash
with an easy and fast CLI application. The intended use of this software is for
automatic downloading of pictures for use cases such as daily wallpapers or application
development tasks.

This project was hacked together rather fast, and code quality is lacking in some places.

## Features/Todo until version 1

* [x] Search term support
* [x] Collection support
* [ ] Specific User
* [ ] Specific User Likes
* [ ] Specific Photo
* [x] Resolution support
* [x] Specify output directory
* [x] Daily/Weekly images

## Building

The project currently builds on Windows and Linux, and should compile/run on Darwin 
as well. You need rust/cargo installed to compile.

```
git clone https://gitlab.com/skylermoore/unsplash-downloader.git
cd unsplash-downloader
cargo build --release
```

## Installing

To build/install the package in one step:

```
cargo install --git https://gitlab.com/skylermoore/unsplash-downloader.git
```

This will install unsplash-downloader into the default cargo path.

Alternativly, once the source is compiled, simply copy the output from target/release/unsplash-downloader into your path, and call `unsplash-downloader --help` from the command line.
